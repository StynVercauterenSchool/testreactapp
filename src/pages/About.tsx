import React from 'react'
import {Link} from 'react-router-dom'
import AboutState from '../interfaces/AboutState'
import axios from 'axios'

export default class About extends React.Component<any, AboutState> {
    constructor(props: any) {
        super(props)
        this.state = {
            books: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/api/books')
            .then(res => {
                this.setState({books: res.data})
            })
    }

    render() {
        return (
            <div>
                {this.state.books.map(character => (
                    <Link key={character.id} to={`/about/${character.id}`} >
                        <h1>{character.title}</h1>
                    </Link>
                ))}
            </div>
        )
    }
}