import React from 'react'
import Formulier from '../components/Formulier'
import MyInfo from '../components/MyInfo'
import Footer from '../components/Footer'
import Login from '../components/Login'
import Character from '../components/Character'

export default function Home() {
    return (
        <div>
            <Formulier />
            <Character />
            <Login />
            <MyInfo text="Train 1" />
            <Footer />
        </div>
    )
}