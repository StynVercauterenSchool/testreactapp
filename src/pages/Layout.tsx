import React from 'react'
import '../App.css'
import Navbar from '../components/Navbar'

import Home from './Home'
import About from './About'
import RoomDetail from './RoomDetail'

import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

export default function Layout() {
  const items = [{key: 1, content: 'Home', page: ''}, {key: 2, content: 'About', page: 'about'}, {key: 3, content: 'Item 3', page: ''}, {key: 4, content: 'Item 4', page: ''}, {key: 5, content: 'Item 5', page: ''}]
  return (
    <Router>
      <div>
        <Navbar
        items={items}
        />
          
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' exact component={About} />
          <Route path='/about/:id' component={RoomDetail} />
        </Switch>

      </div>      
    </Router>
  );
}
