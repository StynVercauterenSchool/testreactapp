import React from 'react'
import RoomDetailState from '../interfaces/RoomDetailState'
import RoomDetailProps from '../interfaces/RoomDetailProps'
import axios from 'axios'

export default class RoomDetail extends React.Component<RoomDetailProps, RoomDetailState> {
    constructor(props: any) {
        super(props)
        this.state = {
            book: {id: props.match.params.id, title: '', description: '', author: ''}
        }
    }
        
    componentDidMount() {
        axios.get('http://localhost:4000/api/books/' + this.state.book.id )
            .then(res => {
                this.setState({book: res.data})
            })
    }

    render() {
        return (
            <div>
                <h1>{this.state.book.title}</h1>
            </div>
        )
    }
}