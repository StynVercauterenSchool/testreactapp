import React from 'react'
import LoginState from '../interfaces/LoginState'
import { Button } from 'semantic-ui-react'

export default class Login extends React.Component<any, LoginState>{
    constructor(props: any) {
        super(props)
        this.state = {
            isLoggedin: false
        }
        this.changeState = this.changeState.bind(this)
    }

    render() {
        const buttonText = this.state.isLoggedin ? 'Log out' :'Log in'
        const displayText = this.state.isLoggedin ? 'User is logged in' : 'User is not logged in' 
        /*
        const loggedInStyle = {
            fontStyle: "italic",
            color: "CDCDCD",
            textDecoration: "line-trough"
        }
        */
        return (
            <div>
                {<p>{displayText}</p>}
                <Button onClick={this.changeState}>{buttonText}</Button>
            </div>
        )
    }

    changeState() {
        //this.state.isLoggedin ? this.setState({isLoggedin: false}) : this.setState({isLoggedin: true})
        this.setState(prevState => {
            return {
                isLoggedin: !prevState.isLoggedin
            }
        })
    }
}