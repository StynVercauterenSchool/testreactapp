import React from 'react'
import FormulierState from '../interfaces/FormulierState'
import { Button, Checkbox, Form, Dropdown, Label } from 'semantic-ui-react'

export default class Formulier extends React.Component<any, FormulierState> {
    constructor(props: any) {
        super(props)
        this.state = {
            name: 'Ash',
            password: 'Ketchum',
            isTrue: false,
            gender: '',
            color: 'Paars',
            age: 0,
            options: [
                {
                    key: 1,
                    text: 'Paars',
                    value: 'Paars'
                },
                {
                    key: 2,
                    text: 'Zilver',
                    value: 'Zilver'
                }
            ]
        }
    }

    submitToServer() {
        console.log('Submit to server')
    }

    render() {
        return (
        <Form onSubmit={this.submitToServer}>
            <Form.Field>
                <h2>Name</h2>
                <Form.Input
                value={this.state.name}
                type='text'
                onChange={e => this.setState({ name: e.target.value})} 
                />

                <Label>{this.state.name}</Label>                
            </Form.Field>

            <Form.Field>
                <h2>Password</h2>
                <Form.Input
                value={this.state.password}
                type='password'
                onChange={e => this.setState({ password: e.target.value})} 
                />

                <Label>{this.state.password}</Label>                
            </Form.Field>

            <Form.Field>
                <h2>Age</h2>
                <Form.Input
                value={this.state.age}
                type='number'
                onChange={e => this.setState({ age: parseInt( e.target.value)})} 
                />

                <Label>{this.state.age}</Label>                
            </Form.Field>

            <Form.Field>
                <Checkbox toggle
                label='I agree to the Terms and Conditions'
                checked={this.state.isTrue}
                onChange={() => this.setState((prevState) => ({ isTrue: !prevState.isTrue }))}
                />
            </Form.Field>

            <h2>Gender</h2>
            <Form.Group>
                <Form.Radio
                label='Male'
                onChange={() => this.setState({ gender: 'male'})} 
                />

                <Form.Radio
                label='Female'
                onChange={() => this.setState({ gender: 'female'})} 
                />

                <Form.Radio
                label='Other'
                onChange={() => this.setState({ gender: 'other'})} 
                />

                <Label>{this.state.gender}</Label>                
            </Form.Group>

            <h2>Favourite color</h2>
            <Form.Group>
                <Dropdown
                placeholder='Choose one'
                fluid
                selection
                options={this.state.options}
                onChange={(e, { value }) => this.setState({ color: value}) }
                >
                </Dropdown>

                <Label>{this.state.color}</Label>                
            </Form.Group>

            <Button type='submit'>Click</Button>
        </Form>
        )
    }
}