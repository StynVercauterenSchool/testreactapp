import React from 'react'
import characterState from '../interfaces/CharacterState'
import axios from 'axios'

export default class Character extends React.Component<any, characterState> {
    constructor(props: any) {
        super(props)
        this.state = {
            loading: true,
            character: { name: '' }
        }
    }

    componentDidMount() {
        axios.get('https://swapi.co/api/people/1')
        .then(res => {
            this.setState({loading: false, character: res.data})
        })
    }

    render() {
        const tekst = this.state.loading ? 'Jedi loading' : this.state.character.name
        return (
            <div>
                <p>{tekst}</p>
            </div>
        )
    }
}