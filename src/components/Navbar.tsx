import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import { Menu } from 'semantic-ui-react'
import NavbarProps from '../interfaces/NavbarProps'
import NavbarState from '../interfaces/NavbarState'

export default class Navbar extends Component<NavbarProps, NavbarState> {
    constructor(props: any) {
        super(props)
        this.state = {
            activeItem: ''
        }
    }
    
    render() {
        return (
         <Menu>
             {
             this.props.items.map((item) => {
                 return (
                    <Menu.Item as={Link}
                    key={item.key}
                    name={item.content}
                    active={this.state.activeItem === item.content}
                    to={`/${item.page}`}
                    >{item.content}</Menu.Item>                       
                 )
             })
             }
        </Menu>
        )
    }
}