import React from 'react'
import MyInfoState from '../interfaces/MyInfoState';
import { List, Button } from 'semantic-ui-react'
import { format } from 'date-fns'

export default class MyInfo extends React.Component<any, MyInfoState> {
    getTekstInCaps() {
        return this.state.intialText.toUpperCase();
    }

    makeTrainsCaps() {
        this.setState({trains: "I don't like trains"})
    }

    click() {
        this.setState(prevState => {
            return {
                counter: prevState.counter + 1
            }
        })
    }

    constructor(props: any) {
        super(props)
        this.state = {
            intialText: 'Train 4',
            trains: 'I like trains',
            counter: 0
        }
        this.makeTrainsCaps = this.makeTrainsCaps.bind(this)
        this.click = this.click.bind(this)
    }

    render() {
        const date = new Date()
        
        const styles = {
            color: 'red',
            backgroundColor: 'blue'
        }
        return (
          <div>
              <h1>Styn</h1>
              <p>{this.state.trains}</p>
                <List bulleted>
                    <List.Item className="lijst">{this.props.text}</List.Item>
                    <List.Item className="lijst">Train 2</List.Item>
                    <List.Item className="lijst">Train 3</List.Item>
                    <List.Item className="lijst">{this.getTekstInCaps()}</List.Item>
                </List>
        <h1 style={styles}>het is {date.getHours()}</h1>


        <h2>{format(new Date(), "'Today is ' iiii 'the ' d'th in the month of ' MMMM")}</h2>


        <h1>{this.state.counter}</h1>
        <Button onClick={this.makeTrainsCaps}>Klik</Button>
        <Button onClick={this.click}>Counter</Button>
        </div>
    ) 
    }
}