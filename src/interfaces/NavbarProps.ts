export default interface NavbarProps {
    items: {
        key: number,
        content: string,
        page: string
    }[]
}