export default interface AboutState{
    books: { id: number, title: string, description: string, author: string }[]
}