export default interface FormulierState {
    name: string,
    password: string,
    age: number,
    isTrue: boolean,
    gender: string,
    color: any,
    options: {key: number, text: string, value: string}[]
}