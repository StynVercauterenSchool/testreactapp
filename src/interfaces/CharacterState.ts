export default interface characterState {
    loading: boolean,
    character: {name: string}
}