export default interface RoomDetailState{
    book: { id: number, title: string, description: string, author: string }
}